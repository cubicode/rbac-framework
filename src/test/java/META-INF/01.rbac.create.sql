-- -----------------------------------------------------
-- Table `rbac`.`operation_type`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rbac`.`operation_type` (
  `opt_id` INT NOT NULL AUTO_INCREMENT ,
  `opt_name` VARCHAR(100) NOT NULL ,
  `opt_order` INT(2) NOT NULL ,
  PRIMARY KEY (`opt_id`) ,
  UNIQUE INDEX `ope_id_UNIQUE` (`opt_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rbac`.`object_type`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rbac`.`object_type` (
  `obt_id` INT NOT NULL ,
  `obt_name` VARCHAR(100) NOT NULL ,
  `obt_order` INT(2) NOT NULL ,
  PRIMARY KEY (`obt_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rbac`.`application`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rbac`.`application` (
  `app_id` INT NOT NULL ,
  `app_name` VARCHAR(100) NOT NULL ,
  `app_order` INT(2) NOT NULL ,
  PRIMARY KEY (`app_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rbac`.`permission`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rbac`.`permission` (
  `opt_id` INT NOT NULL ,
  `obt_id` INT NOT NULL ,
  `app_id` INT NOT NULL ,
  PRIMARY KEY (`opt_id`, `obt_id`, `app_id`) ,
  CONSTRAINT `fk_permission_operation_type1`
    FOREIGN KEY (`opt_id` )
    REFERENCES `rbac`.`operation_type` (`opt_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_permission_object_type1`
    FOREIGN KEY (`obt_id` )
    REFERENCES `rbac`.`object_type` (`obt_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_permission_application1`
    FOREIGN KEY (`app_id` )
    REFERENCES `rbac`.`application` (`app_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `rbac`.`role`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rbac`.`role` (
  `rol_id` INT(2) NOT NULL AUTO_INCREMENT ,
  `rol_name` VARCHAR(50) NOT NULL ,
  `rol_description` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`rol_id`) );


-- -----------------------------------------------------
-- Table `rbac`.`role_permission`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rbac`.`role_permission` (
  `rol_id` INT(2) NOT NULL ,
  `per_opt_id` INT NOT NULL ,
  `per_obt_id` INT NOT NULL ,
  `per_app_id` INT NOT NULL ,
  PRIMARY KEY (`rol_id`, `per_opt_id`, `per_obt_id`, `per_app_id`) ,
  CONSTRAINT `fk_role_permission_role_id`
    FOREIGN KEY (`rol_id` )
    REFERENCES `rbac`.`role` (`rol_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_permission_permission1`
    FOREIGN KEY (`per_opt_id` , `per_obt_id` , `per_app_id` )
    REFERENCES `rbac`.`permission` (`opt_id` , `obt_id` , `app_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `rbac`.`status`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rbac`.`status` (
  `sta_id` INT(2) NOT NULL ,
  `sta_name` VARCHAR(50) NOT NULL ,
  `sta_order` INT(2) NOT NULL ,
  PRIMARY KEY (`sta_id`) );


-- -----------------------------------------------------
-- Table `rbac`.`user`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rbac`.`user` (
  `usr_id` BIGINT NOT NULL AUTO_INCREMENT ,
  `usr_email` VARCHAR(100) NOT NULL ,
  `usr_first_name` VARCHAR(100) NOT NULL ,
  `usr_last_name` VARCHAR(100) NOT NULL ,
  `usr_password` VARCHAR(255) NOT NULL ,
  `usr_create_date` DATE NOT NULL ,
  `usr_update_date` DATE NULL ,
  `usr_last_login` DATE NULL ,
  `sta_id` INT(2) NOT NULL ,
  `rol_id` INT(2) NOT NULL ,
  PRIMARY KEY (`usr_id`) ,
  CONSTRAINT `fk_{CF44CDB1-F6BC-4240-A501-3C3A5AB0902D}`
    FOREIGN KEY (`sta_id` )
    REFERENCES `rbac`.`status` (`sta_id` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_{81176270-CE88-4133-A020-F6A750A08548}`
    FOREIGN KEY (`rol_id` )
    REFERENCES `rbac`.`role` (`rol_id` ));


-- -----------------------------------------------------
-- Table `rbac`.`action`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rbac`.`action` (
  `act_id` INT NOT NULL AUTO_INCREMENT ,
  `act_name` VARCHAR(100) NOT NULL ,
  `act_url` VARCHAR(255) NOT NULL ,
  `act_order` INT(2) NOT NULL ,
  `act_access_direct` TINYINT(1) NOT NULL ,
  `act_required_login` TINYINT(1) NOT NULL ,
  `per_opt_id` INT NOT NULL ,
  `per_obt_id` INT NOT NULL ,
  `per_app_id` INT NOT NULL ,
  PRIMARY KEY (`act_id`) ,
  CONSTRAINT `fk_action_permission1`
    FOREIGN KEY (`per_opt_id` , `per_obt_id` , `per_app_id` )
    REFERENCES `rbac`.`permission` (`opt_id` , `obt_id` , `app_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Data for table `rbac`.`operation_type`
-- -----------------------------------------------------
INSERT INTO `rbac`.`operation_type` (`opt_id`, `opt_name`, `opt_order`) VALUES (1, 'crear', 1);
INSERT INTO `rbac`.`operation_type` (`opt_id`, `opt_name`, `opt_order`) VALUES (2, 'actualizar', 2);
INSERT INTO `rbac`.`operation_type` (`opt_id`, `opt_name`, `opt_order`) VALUES (3, 'eliminar', 3);
INSERT INTO `rbac`.`operation_type` (`opt_id`, `opt_name`, `opt_order`) VALUES (4, 'listar', 4);

COMMIT;

-- -----------------------------------------------------
-- Data for table `rbac`.`status`
-- -----------------------------------------------------
INSERT INTO `rbac`.`status` (`sta_id`, `sta_name`, `sta_order`) VALUES (1, 'activo', 1);
INSERT INTO `rbac`.`status` (`sta_id`, `sta_name`, `sta_order`) VALUES (2, 'inactivo', 2);
INSERT INTO `rbac`.`status` (`sta_id`, `sta_name`, `sta_order`) VALUES (3, 'eliminado', 3);

COMMIT;
