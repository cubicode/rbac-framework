#findByPrimaryKey
select * from rbac.user where USR_id=:id;

#create
insert into rbac.user(
	usr_first_name,
	usr_last_name,
	usr_password,
	usr_email,
	usr_create_date,
	usr_update_date,
	sta_id,
	usr_last_login,
	rol_id) 
	values (
	:firstName,
	:lastName,
	:password,
	:email,
	:createDate,
	:updateDate,
	:status,
	:lastLogin,
	:role);
	
#getAll
select * from rbac.user;