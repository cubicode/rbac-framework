#findByPrimaryKey
select 
	rol_id,
	rol_name,
	rol_description
from rbac.role 
where rol_id = :id;

#create
insert into rbac.role(
	rol_name,
	rol_description) 
values (
	:name,
	:description);
	
#getAll
select 
	rol_id,
	rol_name,
	rol_description  
from rbac.role;