#findByPrimaryKey
select 
	app_id,
	obt_id,
	opt_id
from rbac.permission 
where 
	app_id = :application and
	obt_id = :objectType and
	opt_id = :operationType;

#create
insert into rbac.permission(
	app_id,
	obt_id,
	opt_id) 
values (
	:application,
	:objectType,
	:operationType);
	
#getAll
select 
	app_id,
	obt_id,
	opt_id 
from rbac.permission;