#findByRoleAndApplication
select 
	rol_id,
	per_app_id,
	per_obt_id,
	per_opt_id
from rbac.role_permission 
where 
	rol_id = :role and
	per_app_id = :application;
	
