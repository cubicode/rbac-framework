#getAll
select 
	act_id,
	act_name, 
	act_order,
	act_url,
	act_access_direct,
	act_required_login,
	per_obt_id,
	per_opt_id,
	per_app_id 
from rbac.action;

