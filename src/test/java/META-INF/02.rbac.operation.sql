INSERT INTO rbac.application (app_ID, app_NAME, app_order) VALUES (1, 'web', 1);
INSERT INTO rbac.application (app_ID, app_NAME, app_order) VALUES (2, 'admin', 2);
INSERT INTO rbac.object_type (obt_ID, obt_NAME, obt_order) VALUES (1, 'Usuarios', 1);
INSERT INTO rbac.object_type (obt_ID, obt_NAME, obt_order) VALUES (2, 'Permisos', 2);
INSERT INTO rbac.permission (opt_ID, obt_id, app_id) VALUES (1, 1, 2);
INSERT INTO rbac.permission (opt_ID, obt_id, app_id) VALUES (2, 1, 2);
INSERT INTO rbac.permission (opt_ID, obt_id, app_id) VALUES (3, 1, 2);
INSERT INTO rbac.permission (opt_ID, obt_id, app_id) VALUES (4, 1, 2);
INSERT INTO rbac.permission (opt_ID, obt_id, app_id) VALUES (1, 1, 1);
INSERT INTO rbac.ROLE (ROL_ID, ROL_NAME, ROL_DESCRIPTION) VALUES (1, 'administrator', 'rol administrador');
INSERT INTO rbac.ROLE (ROL_ID, ROL_NAME, ROL_DESCRIPTION) VALUES (2, 'operator', 'rol operador');
INSERT INTO rbac.ROLE (ROL_ID, ROL_NAME, ROL_DESCRIPTION) VALUES (3, 'guest', 'rol invitado');
INSERT INTO rbac.action (act_ID, act_NAME, act_order, act_url, act_access_direct, act_required_login, per_opt_id, per_obt_id, per_app_id) 
VALUES (1, '', 1, '', 1, 0, 1, 1, 2);
INSERT INTO rbac.action (act_ID, act_NAME, act_order, act_url, act_access_direct, act_required_login, per_opt_id, per_obt_id, per_app_id) 
VALUES (2, '', 1, '', 1, 0, 1, 1, 1);
INSERT INTO rbac.ROLE_PERMISSION (ROL_ID, PER_opt_ID, PER_obt_ID, PER_app_ID) VALUES (1, 1, 1, 2);
INSERT INTO rbac.ROLE_PERMISSION (ROL_ID, PER_opt_ID, PER_obt_ID, PER_app_ID) VALUES (1, 2, 1, 2);
INSERT INTO rbac.ROLE_PERMISSION (ROL_ID, PER_opt_ID, PER_obt_ID, PER_app_ID) VALUES (1, 3, 1, 2);
INSERT INTO rbac.ROLE_PERMISSION (ROL_ID, PER_opt_ID, PER_obt_ID, PER_app_ID) VALUES (1, 4, 1, 2);
INSERT INTO rbac.user (USR_ID, USR_FIRST_NAME, USR_LAST_NAME, USR_PASSWORD, USR_EMAIL, USR_CREATE_DATE, USR_UPDATE_DATE, STA_ID, USR_LAST_LOGIN, ROL_ID) 
VALUES (1, 'Admin first name', 'Admin last name', '12345', 'admin@admin.com', '2012-05-06', NULL, 1, NULL, 1),




