package co.cubicode.rbacframework.bizTest;

import junit.framework.Assert;

import org.junit.Test;

import co.cubicode.jdbcframework.Catalog;
import co.cubicode.jdbcframework.exceptions.ObjectNotFoundException;
import co.cubicode.rbacframework.ApiTest;
import co.cubicode.rbacframework.biz.PermissionBiz;
import co.cubicode.rbacframework.constants.ApplicationConstants;
import co.cubicode.rbacframework.constants.ObjectTypeConstants;
import co.cubicode.rbacframework.models.Permission;
import co.cubicode.rbacframework.models.catalogs.Application;
import co.cubicode.rbacframework.models.catalogs.ObjectType;
import co.cubicode.rbacframework.models.catalogs.OperationType;

public class PermissionTest extends ApiTest {

	@Test
	public void findByPrimaryKeyTest() {
		Application application = Catalog.getElement(Application.class, ApplicationConstants.ADMIN);
		ObjectType objectType = Catalog.getElement(ObjectType.class, ObjectTypeConstants.USER);
		OperationType operationType = Catalog.getElement(OperationType.class, OperationType.CREATE);
		Permission permission = null;
		try {
			permission = PermissionBiz.findByPrimaryKey(application, objectType, operationType);
		} catch (ObjectNotFoundException e) {
			e.printStackTrace();
			Assert.fail();
		}
		Assert.assertNotNull(permission);
		System.out.println(permission);
	}

}
