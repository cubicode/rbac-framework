package co.cubicode.rbacframework.bizTest;

import org.junit.Test;

import co.cubicode.jdbcframework.exceptions.ObjectNotFoundException;
import co.cubicode.rbacframework.ApiTest;
import co.cubicode.rbacframework.biz.RoleBiz;
import co.cubicode.rbacframework.models.Role;

public class RoleTest extends ApiTest {

	@Test
	public void findByPrimaryKeyTest() throws ObjectNotFoundException {
		Role role = RoleBiz.findByPrimaryKey(1);
		System.out.println(role);
	}

}
