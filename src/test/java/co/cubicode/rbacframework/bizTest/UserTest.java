package co.cubicode.rbacframework.bizTest;

import java.util.Calendar;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import co.cubicode.jdbcframework.exceptions.ObjectNotFoundException;
import co.cubicode.jdbcframework.exceptions.ObjectUpdateException;
import co.cubicode.rbacframework.ApiTest;
import co.cubicode.rbacframework.biz.UserBiz;
import co.cubicode.rbacframework.models.Role;
import co.cubicode.rbacframework.models.Status;
import co.cubicode.rbacframework.models.User;
import co.cubicode.utils.lib.PasswordUtils;

public class UserTest extends ApiTest {

	@Test
	public void findByPrimaryKeyTest() {
		User user = null;
		try {
			user = UserBiz.findByPrimaryKey(1L);
		} catch (ObjectNotFoundException e) {
			e.printStackTrace();
			Assert.fail();
		}
		Assert.assertNotNull(user);
		System.out.println(user);
	}

	@Test
	public void getAllTest() {
		List<User> users = null;
		try {
			users = UserBiz.getAll();
		} catch (ObjectNotFoundException e) {
			e.printStackTrace();
			Assert.fail();
		}
		Assert.assertNotNull(users);
		for (User user : users) {
			System.out.println(user);
		}
	}

	@Test
	public void createTest() {
		User user = new User();
		user.setEmail("email");
		user.setFirstName("primer nombre");
		user.setLastName("apellido");
		user.setPassword("admin");
		user.setCreateDate(Calendar.getInstance().getTime());
		user.setUpdateDate(Calendar.getInstance().getTime());
		user.setLastLogin(Calendar.getInstance().getTime());
		user.setStatus(Status.getElement(Status.class, Status.ACTIVE));
		Role role = new Role();
		role.setId(1);
		user.setRole(role);
		try {
			user = UserBiz.create(user);
		} catch (ObjectUpdateException e) {
			e.printStackTrace();
			Assert.fail();
		}
		Assert.assertNotNull(user);
		System.out.println(user);
	}

	@Test
	public void createSuperUserTest() {
		User user = new User();
		user.setEmail("email");
		user.setFirstName("Sin nombre");
		user.setLastName("Sin apellido");
		String password = "Jfsebe4tbGvr";
		user.setPassword(PasswordUtils.hashPassword(password));
		user.setCreateDate(Calendar.getInstance().getTime());
		user.setStatus(Status.getElement(Status.class, Status.ACTIVE));
		Role role = new Role();
		role.setId(1);
		user.setRole(role);
		try {
			user = UserBiz.create(user);
		} catch (ObjectUpdateException e) {
			e.printStackTrace();
			Assert.fail();
		}
		Assert.assertNotNull(user);
		System.out.println(user);
	}
	
}
