package co.cubicode.rbacframework;

import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.ConfigurationException;

import org.apache.logging.log4j.core.config.Configurator;
import org.h2.tools.RunScript;
import org.h2.tools.Server;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import co.cubicode.jdbcframework.DatabaseFactory;

public class ApiTest {

	private static final String URL = "jdbc:h2:mem:test;INIT=CREATE SCHEMA IF NOT EXISTS rbac;DB_CLOSE_DELAY=-1;MVCC=true";

	@BeforeClass
	public static void init() throws ConfigurationException, SQLException, ClassNotFoundException {
		Server.createTcpServer().start();
		initDatabase();
		Configurator.initialize(null, null, "META-INF/log4j2.xml");
		DatabaseFactory.init("META-INF/settings.properties");
		createDatabaseAndLoadWithDumpData();
		verifyDumpData();
	}

	private static void initDatabase() throws ClassNotFoundException, SQLException {
		Class.forName("org.h2.Driver");
		DriverManager.getConnection(URL, "rbac", "").close();
	}

	private static void verifyDumpData() throws ClassNotFoundException, SQLException {
		Connection con = null;
		Statement stat = null;
		try {
			Class.forName("org.h2.Driver");
			con = DriverManager.getConnection(URL, "rbac", "");
			stat = con.createStatement();
			ResultSet rs = stat.executeQuery("select STA_ID, STA_NAME from rbac.status");
			if (!rs.next()) {
				throw new RuntimeException("La base de datos no cuenta con datos dump");
			}
			rs.close();
		} finally {
			try {
				if (stat != null) {
					stat.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private static void createDatabaseAndLoadWithDumpData() throws ClassNotFoundException, SQLException {
		Statement stat = null;
		Connection con = null;
		PreparedStatement prep = null;
		try {
			Class.forName("org.h2.Driver");
			con = DriverManager.getConnection(URL, "rbac", "");
			stat = con.createStatement();
			InputStreamReader isr = new InputStreamReader(ApiTest.class.getClassLoader().getResourceAsStream("META-INF/01.rbac.create.sql"));
			RunScript.execute(con, isr);
			con.commit();
			isr = new InputStreamReader(ApiTest.class.getClassLoader().getResourceAsStream("META-INF/02.rbac.operation.sql"));
			RunScript.execute(con, isr);
			con.commit();
		} finally {
			try {
				if (prep != null) {
					prep.close();
				}
				if (stat != null) {
					stat.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private static void dropSchema() throws SQLException {
		Statement stat = null;
		Connection con = null;
		PreparedStatement prep = null;
		try {
			Class.forName("org.h2.Driver");
			con = DriverManager.getConnection(URL, "rbac", "");
			stat = con.createStatement();
			stat.execute("DROP SCHEMA rbac");
			con.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (prep != null) {
					prep.close();
				}
				if (stat != null) {
					stat.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@AfterClass
	public static void end() throws SQLException {
		DatabaseFactory.closeAll();
		dropSchema();
		Server.createTcpServer().shutdown();
	}

}
