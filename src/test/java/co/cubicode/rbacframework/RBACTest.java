package co.cubicode.rbacframework;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import co.cubicode.jdbcframework.Catalog;
import co.cubicode.jdbcframework.exceptions.ObjectNotFoundException;
import co.cubicode.rbacframework.biz.PermissionBiz;
import co.cubicode.rbacframework.biz.RoleBiz;
import co.cubicode.rbacframework.biz.RolePermissionBiz;
import co.cubicode.rbacframework.biz.UserBiz;
import co.cubicode.rbacframework.constants.ApplicationConstants;
import co.cubicode.rbacframework.models.Permission;
import co.cubicode.rbacframework.models.Role;
import co.cubicode.rbacframework.models.User;
import co.cubicode.rbacframework.models.catalogs.Action;
import co.cubicode.rbacframework.models.pack.RolePermission;

public class RBACTest extends ApiTest {

	@Test
	public void rbacTest() throws ObjectNotFoundException {
		User user = UserBiz.findByPrimaryKey(1L);
		Role role = RoleBiz.findByPrimaryKey(user.getRole().getId());
		List<RolePermission> ids = RolePermissionBiz.findByRoleAndApplication(role, ApplicationConstants.ADMIN);
		List<Permission> permissions = new ArrayList<Permission>();
		for (RolePermission rolePermission : ids) {
			Permission permission = PermissionBiz.findByPrimaryKey(rolePermission.getApplicationId(), rolePermission.getObjectTypeId(),
					rolePermission.getOperationTypeId());
			permissions.add(permission);
		}
		role.setPermissions(permissions);
		user.setRole(role);
		System.out.println(user);

		if (user.hasRole("administrator")) {
			System.out.println("Es administrador");
		} else if (user.hasRole("operator")) {
			System.out.println("Es operador");
			Assert.fail();
		} else if (user.hasRole("guest")) {
			System.out.println("Es invitado");
			Assert.fail();
		} else {
			System.out.println("No tiene rol");
			Assert.fail();
		}
		
		Action action = Catalog.getElement(Action.class, "2");
		Permission permission = new Permission(action.getApplication(), action.getObjectType(), action.getOperationType());
		if (user.hasPermission(permission)) {
			Assert.fail();
		} else {
			System.out.println("No tiene permisos en web");
		}
	}

}
