package co.cubicode.rbacframework.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import co.cubicode.rbacframework.models.catalogs.SubjectStatus;

public class User extends Subject {

	// atributos
	private static final long serialVersionUID = -1689568727819614127L;

	public User() {
		role = new Role();
	}

	@Override
	public void loadDatabaseObject(ResultSet resultSet) throws SQLException {
		id = resultSet.getLong("usr_id");
		firstName = resultSet.getString("usr_first_name");
		lastName = resultSet.getString("usr_last_name");
		password = resultSet.getString("usr_password");
		email = resultSet.getString("usr_email");
		createDate = resultSet.getDate("usr_create_date");
		updateDate = resultSet.getDate("usr_update_date");
		status = SubjectStatus.getElement(Status.class, resultSet.getString("sta_id"));
		lastLogin = resultSet.getDate("usr_last_login");
		role.setId(resultSet.getInt("rol_id"));
	}

	@Override
	public void map(HashMap<String, Object> hashMap) {
		hashMap.put("id", id);
		hashMap.put("firstName", firstName);
		hashMap.put("lastName", lastName);
		hashMap.put("password", password);
		hashMap.put("email", email);
		hashMap.put("createDate", createDate);
		hashMap.put("updateDate", updateDate);
		hashMap.put("status", status.getId());
		hashMap.put("lastLogin", lastLogin);
		hashMap.put("role", role.getId());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ID: ").append(this.id).append(", ");
		sb.append("Firstname: ").append(this.firstName).append(", ");
		sb.append("Lastname: ").append(this.lastName).append(", ");
		sb.append("Email: ").append(this.email).append(", ");
		sb.append("Status: ").append(this.status).append(", ");
		sb.append("Role: ").append(this.role);
		return sb.toString();
	}

}
