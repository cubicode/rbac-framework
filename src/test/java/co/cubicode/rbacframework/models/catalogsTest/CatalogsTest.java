package co.cubicode.rbacframework.models.catalogsTest;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.List;

import org.junit.Test;

import co.cubicode.jdbcframework.Catalog;
import co.cubicode.rbacframework.ApiTest;
import co.cubicode.rbacframework.TestUtils;

public class CatalogsTest extends ApiTest {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public <T extends Catalog> void getAllTest() throws ClassNotFoundException, IOException {
		String packageName = "co.cubicode.rbacframework.models.catalogs";
		Class[] classes = TestUtils.getClasses(packageName);
		for (Class c : classes) {
			if(Modifier.isAbstract(c.getModifiers())) {
				continue;
			}
			List<T> list = Catalog.getElements(c);
			System.out.println(c);
			for (T obj : list) {
				System.out.println(obj.toString());
			}
		}
	}

}
