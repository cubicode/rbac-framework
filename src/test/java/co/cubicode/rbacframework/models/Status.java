package co.cubicode.rbacframework.models;

import java.sql.ResultSet;
import java.sql.SQLException;

import co.cubicode.rbacframework.models.catalogs.SubjectStatus;

public class Status extends SubjectStatus {

	// atributos
	private static final long serialVersionUID = 2569133525677346224L;

	public void loadDatabaseObject(ResultSet resultSet) throws SQLException {
		id = resultSet.getString("sta_id");
		name = resultSet.getString("sta_name");
		order = resultSet.getInt("sta_order");
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ID: ").append(this.id).append(", ");
		sb.append("Name: ").append(this.name).append(", ");
		sb.append("Order: ").append(this.order);
		return sb.toString();
	}

}
