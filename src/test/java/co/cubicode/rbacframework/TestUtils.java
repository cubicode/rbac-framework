package co.cubicode.rbacframework;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Clase con utilidades para las pruebas del API
 * 
 * @author juanku
 * 
 */
public class TestUtils {

	/**
	 * Scanea todas las clases que pertenecen a un paquete
	 * 
	 * @param packageName
	 *            el nombre del paquete
	 * @return las clases encontradas
	 * @throws ClassNotFoundException
	 *             en caso de no encontrar una clase
	 * @throws IOException
	 *             en caso de no poder leer una clase
	 */
	@SuppressWarnings("rawtypes")
	public static Class[] getClasses(String packageName) throws ClassNotFoundException, IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<File>();
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}
		ArrayList<Class> classes = new ArrayList<Class>();
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName));
		}
		return classes.toArray(new Class[classes.size()]);
	}

	/**
	 * Método recursivo usado para obtener todas las clases de un directorio y sus subdirectorios
	 * 
	 * @param directory
	 *            directorio raiz
	 * @param packageName
	 *            nombre del paquete
	 * @return las clases encontradas
	 * @throws ClassNotFoundException
	 *             en caso de no encontrar una clase
	 */
	@SuppressWarnings("rawtypes")
	public static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
		List<Class> classes = new ArrayList<Class>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				assert !file.getName().contains(".");
				classes.addAll(findClasses(file, packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class")) {
				classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
			}
		}
		return classes;
	}

}
