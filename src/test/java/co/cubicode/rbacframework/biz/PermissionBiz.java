package co.cubicode.rbacframework.biz;

import co.cubicode.jdbcframework.DatabaseFactory;
import co.cubicode.jdbcframework.SQL;
import co.cubicode.jdbcframework.SQLQuery;
import co.cubicode.jdbcframework.exceptions.ObjectNotFoundException;
import co.cubicode.rbacframework.models.Permission;
import co.cubicode.rbacframework.models.catalogs.Application;
import co.cubicode.rbacframework.models.catalogs.ObjectType;
import co.cubicode.rbacframework.models.catalogs.OperationType;

public class PermissionBiz {

	public static Permission findByPrimaryKey(Application application, ObjectType objectType, OperationType operationType)
			throws ObjectNotFoundException {
		return findByPrimaryKey(application.getId(), objectType.getId(), operationType.getId());
	}

	public static Permission findByPrimaryKey(Integer application, Integer objectType, Integer operationType) throws ObjectNotFoundException {
		return findByPrimaryKey(application.toString(), objectType.toString(), operationType.toString());
	}

	public static Permission findByPrimaryKey(String application, String objectType, String operationType) throws ObjectNotFoundException {
		DatabaseFactory instance = DatabaseFactory.getInstance("rbac");
		SQLQuery sql = SQL.getSQL(Permission.class, "findByPrimaryKey");
		sql.setParameter("application", application);
		sql.setParameter("objectType", objectType);
		sql.setParameter("operationType", operationType);
		return instance.query(Permission.class, sql);
	}

}
