package co.cubicode.rbacframework.biz;

import java.util.List;

import co.cubicode.jdbcframework.DatabaseFactory;
import co.cubicode.jdbcframework.SQL;
import co.cubicode.jdbcframework.SQLQuery;
import co.cubicode.jdbcframework.exceptions.DatabaseFactoryException;
import co.cubicode.jdbcframework.exceptions.ObjectNotFoundException;
import co.cubicode.jdbcframework.exceptions.ObjectUpdateException;
import co.cubicode.rbacframework.models.User;

public class UserBiz {

	public static User findByPrimaryKey(Long id) throws ObjectNotFoundException {
		DatabaseFactory instance = DatabaseFactory.getInstance("rbac");
		SQLQuery sql = SQL.getSQL(User.class, "findByPrimaryKey");
		sql.setParameter("id", id);
		return instance.query(User.class, sql);
	}

	public static List<User> getAll() throws ObjectNotFoundException {
		DatabaseFactory instance = DatabaseFactory.getInstance("rbac");
		SQLQuery sql = SQL.getSQL(User.class, "getAll");
		return instance.queryForList(User.class, sql);
	}
	
	public static User create(User user) throws ObjectUpdateException {
		DatabaseFactory instance = DatabaseFactory.getInstance("rbac");
		SQLQuery sql = SQL.getSQL(User.class, "create");
		Long id = (Long) instance.update(user, sql);
		if (id == null) {
			throw new DatabaseFactoryException("Ocurrio un error al obtener el id del objeto usuario");
		}
		user.setId(id);
		return user;
	}
	
}
