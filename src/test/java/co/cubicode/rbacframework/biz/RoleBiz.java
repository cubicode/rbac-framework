package co.cubicode.rbacframework.biz;

import co.cubicode.jdbcframework.DatabaseFactory;
import co.cubicode.jdbcframework.SQL;
import co.cubicode.jdbcframework.SQLQuery;
import co.cubicode.jdbcframework.exceptions.ObjectNotFoundException;
import co.cubicode.rbacframework.models.Role;

public class RoleBiz {

	public static Role findByPrimaryKey(Integer id) throws ObjectNotFoundException {
		DatabaseFactory instance = DatabaseFactory.getInstance("rbac");
		SQLQuery sql = SQL.getSQL(Role.class, "findByPrimaryKey");
		sql.setParameter("id", id);
		return instance.query(Role.class, sql);
	}

}
