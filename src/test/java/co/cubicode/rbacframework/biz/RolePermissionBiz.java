package co.cubicode.rbacframework.biz;

import java.util.List;

import co.cubicode.jdbcframework.DatabaseFactory;
import co.cubicode.jdbcframework.SQL;
import co.cubicode.jdbcframework.SQLQuery;
import co.cubicode.jdbcframework.exceptions.ObjectNotFoundException;
import co.cubicode.rbacframework.models.Role;
import co.cubicode.rbacframework.models.pack.RolePermission;

public class RolePermissionBiz {

	public static List<RolePermission> findByRoleAndApplication(Role role, String application) throws ObjectNotFoundException {
		DatabaseFactory instance = DatabaseFactory.getInstance("rbac");
		SQLQuery sql = SQL.getSQL(RolePermission.class, "findByRoleAndApplication");
		sql.setParameter("role", role.getId());
		sql.setParameter("application", application);
		return instance.queryForList(RolePermission.class, sql);
	}

}
