package co.cubicode.rbacframework.models;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import net.sf.oval.constraint.NotNull;

import co.cubicode.jdbcframework.Catalog;
import co.cubicode.jdbcframework.DatabaseObject;
import co.cubicode.rbacframework.models.catalogs.Application;
import co.cubicode.rbacframework.models.catalogs.ObjectType;
import co.cubicode.rbacframework.models.catalogs.OperationType;

/**
 * Clase que representa el permiso de un rol
 * 
 * @author juanku
 * 
 */
public class Permission implements DatabaseObject, Serializable {

	private static final long serialVersionUID = 1L;
	// atributos
	@NotNull
	private Application application;
	@NotNull
	private ObjectType objectType;
	@NotNull
	private OperationType operationType;

	public Permission() {
	}

	public Permission(Application application, ObjectType objectType, OperationType operationType) {
		this.application = application;
		this.operationType = operationType;
		this.objectType = objectType;
	}
	
	@Override
	public void loadDatabaseObject(ResultSet resultSet) throws SQLException {
		operationType = Catalog.getElement(OperationType.class, resultSet.getString("opt_id"));
		application = Catalog.getElement(Application.class, resultSet.getString("app_id"));
		objectType = Catalog.getElement(ObjectType.class, resultSet.getString("obt_id"));
	}

	@Override
	public void map(HashMap<String, Object> hashMap) {
		hashMap.put("application", application.getId());
		hashMap.put("objectType", objectType.getId());
		hashMap.put("operationType", operationType.getId());
	}

	// getters & setters
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public ObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(ObjectType objectType) {
		this.objectType = objectType;
	}

	public OperationType getOperationType() {
		return operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("application: ").append(this.application).append(", ");
		sb.append("objectType: ").append(this.objectType).append(", ");
		sb.append("operationType: ").append(this.operationType);
		return sb.toString();
	}

}
