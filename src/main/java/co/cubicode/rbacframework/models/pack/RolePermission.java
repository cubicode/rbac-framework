package co.cubicode.rbacframework.models.pack;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import co.cubicode.jdbcframework.DatabaseObject;

/**
 * Clase que representa la relación rol-permiso
 * 
 * @author juanku
 * 
 */
public class RolePermission implements DatabaseObject {

	// atributos
	private Integer roleId;
	private Integer applicationId;
	private Integer objectTypeId;
	private Integer operationTypeId;

	@Override
	public void loadDatabaseObject(ResultSet resultSet) throws SQLException {
		roleId = resultSet.getInt("rol_id");
		applicationId = resultSet.getInt("per_app_id");
		objectTypeId = resultSet.getInt("per_obt_id");
		operationTypeId = resultSet.getInt("per_opt_id");
	}

	@Override
	public void map(HashMap<String, Object> hashMap) {
		hashMap.put("role", roleId);
		hashMap.put("application", applicationId);
		hashMap.put("objectType", objectTypeId);
		hashMap.put("operationType", operationTypeId);
	}

	// getters & setters
	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getObjectTypeId() {
		return objectTypeId;
	}

	public void setObjectTypeId(Integer objectTypeId) {
		this.objectTypeId = objectTypeId;
	}

	public Integer getOperationTypeId() {
		return operationTypeId;
	}

	public void setOperationTypeId(Integer operationTypeId) {
		this.operationTypeId = operationTypeId;
	}

}
