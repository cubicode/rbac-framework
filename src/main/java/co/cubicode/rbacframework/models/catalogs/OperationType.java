package co.cubicode.rbacframework.models.catalogs;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.cubicode.jdbcframework.Catalog;

/**
 * Clase que representa un tipo de operación (crear, editar, eliminar y listar)
 * 
 * @author juanku
 * 
 */
public class OperationType extends Catalog implements Serializable {

	private static final long serialVersionUID = 7401680323630642819L;
	// valores del catálogo
	public static final String CREATE = "1";
	public static final String UPDATE = "2";
	public static final String DELETE = "3";
	public static final String LIST = "4";

	@Override
	public void loadDatabaseObject(ResultSet resultSet) throws SQLException {
		id = resultSet.getString("opt_id");
		name = resultSet.getString("opt_name");
		order = resultSet.getInt("opt_order");
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(this.id).append(", ");
		sb.append("name: ").append(this.name).append(", ");
		sb.append("order: ").append(this.order).append(", ");
		sb.append("Display Name: ").append(getDisplayName());
		return sb.toString();
	}

}
