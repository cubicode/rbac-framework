package co.cubicode.rbacframework.models.catalogs;

import java.io.Serializable;

import co.cubicode.jdbcframework.Catalog;

/**
 * Catálogo que representa el status de un usuario
 * 
 * @author juanku
 * 
 */
public abstract class SubjectStatus extends Catalog implements Serializable {

	private static final long serialVersionUID = 2018490375334325657L;
	// valores del catálogo
	public static final String ACTIVE = "1";
	public static final String INACTIVE = "2";
	public static final String DELETED = "3";

}
