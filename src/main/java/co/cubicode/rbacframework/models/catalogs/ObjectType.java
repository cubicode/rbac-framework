package co.cubicode.rbacframework.models.catalogs;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.cubicode.jdbcframework.Catalog;

/**
 * Clase que representa un tipo de objeto
 * 
 * @author juanku
 * 
 */
public class ObjectType extends Catalog implements Serializable {

	private static final long serialVersionUID = -1021494698401892014L;

	@Override
	public void loadDatabaseObject(ResultSet resultSet) throws SQLException {
		id = resultSet.getString("obt_id");
		name = resultSet.getString("obt_name");
		order = resultSet.getInt("obt_order");
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(this.id).append(", ");
		sb.append("name: ").append(this.name).append(", ");
		sb.append("order: ").append(this.order).append(", ");
		sb.append("Display Name: ").append(getDisplayName());
		return sb.toString();
	}

}
