package co.cubicode.rbacframework.models.catalogs;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.cubicode.jdbcframework.Catalog;

/**
 * Clase que representa a la aplicación
 * 
 * @author juanku
 * 
 */
public class Application extends Catalog implements Serializable {

	private static final long serialVersionUID = 4885752575469672085L;

	@Override
	public void loadDatabaseObject(ResultSet resultSet) throws SQLException {
		id = resultSet.getString("app_id");
		name = resultSet.getString("app_name");
		order = resultSet.getInt("app_order");
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(this.id).append(", ");
		sb.append("name: ").append(this.name).append(", ");
		sb.append("order: ").append(this.order).append(", ");
		sb.append("Display Name: ").append(getDisplayName());
		return sb.toString();
	}

}
