package co.cubicode.rbacframework.models.catalogs;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.cubicode.jdbcframework.Catalog;

/**
 * Clase que representa una acción, url ó recurso
 * 
 * @author juanku
 * 
 */
public class Action extends Catalog implements Serializable {

	private static final long serialVersionUID = 628973241243304538L;
	// atributos
	private String url;
	private Boolean isAccessDirect;
	private Boolean isLoginRequired;
	private ObjectType objectType;
	private OperationType operationType;
	private Application application;

	@Override
	public void loadDatabaseObject(ResultSet resultSet) throws SQLException {
		id = resultSet.getString("act_id");
		name = resultSet.getString("act_name");
		order = resultSet.getInt("act_order");
		url = resultSet.getString("act_url");
		isAccessDirect = resultSet.getBoolean("act_access_direct");
		isLoginRequired = resultSet.getBoolean("act_required_login");
		application = Catalog.getElement(Application.class, resultSet.getString("per_app_id"));
		setObjectType(Catalog.getElement(ObjectType.class, resultSet.getString("per_obt_id")));
		setOperationType(Catalog.getElement(OperationType.class, resultSet.getString("per_opt_id")));
	}

	// getters & setters
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Boolean getIsAccessDirect() {
		return isAccessDirect;
	}

	public void setIsAccessDirect(Boolean isAccessDirect) {
		this.isAccessDirect = isAccessDirect;
	}

	public Boolean getIsLoginRequired() {
		return isLoginRequired;
	}

	public void setIsLoginRequired(Boolean isLoginRequired) {
		this.isLoginRequired = isLoginRequired;
	}

	public ObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(ObjectType objectType) {
		this.objectType = objectType;
	}

	public OperationType getOperationType() {
		return operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(this.id).append(", ");
		sb.append("name: ").append(this.name).append(", ");
		sb.append("order: ").append(this.order).append(", ");
		sb.append("url: ").append(this.url).append(", ");
		sb.append("accessDirect: ").append(this.isAccessDirect).append(", ");
		sb.append("isLoginRequired: ").append(this.isLoginRequired).append(", ");
		sb.append("objectType: ").append(this.objectType).append(", ");
		sb.append("operationType: ").append(this.operationType).append(", ");
		sb.append("application: ").append(this.application).append(", ");
		sb.append("Display Name: ").append(getDisplayName());
		return sb.toString();
	}

}
