package co.cubicode.rbacframework.models;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sf.oval.constraint.AssertNull;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import co.cubicode.jdbcframework.DatabaseObject;
import co.cubicode.rbacframework.models.catalogs.OperationType;

/**
 * Clase que representa un rol de un usuario
 * 
 * @author juanku
 * 
 */
public class Role implements DatabaseObject, Serializable {

	private static final long serialVersionUID = -760953554003372829L;
	// atributos
	@NotNull(profiles = { OperationType.UPDATE })
	@AssertNull(profiles = { OperationType.CREATE })
	private Integer id;
	@NotNull
	@NotEmpty
	private String name;
	@NotNull
	@NotEmpty
	private String description;
	@NotNull
	private List<Permission> permissions;

	public Role() {
		permissions = new ArrayList<Permission>();
	}

	@Override
	public void loadDatabaseObject(ResultSet resultSet) throws SQLException {
		id = resultSet.getInt("rol_id");
		name = resultSet.getString("rol_name");
		description = resultSet.getString("rol_description");
	}

	@Override
	public void map(HashMap<String, Object> hashMap) {
		hashMap.put("id", id);
		hashMap.put("name", name);
		hashMap.put("description", description);
	}

	// getters & setters
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(this.id).append(", ");
		sb.append("name: ").append(this.name).append(", ");
		sb.append("description: ").append(this.description).append(", ");
		sb.append("permissions: ").append(this.permissions);
		return sb.toString();
	}

}
