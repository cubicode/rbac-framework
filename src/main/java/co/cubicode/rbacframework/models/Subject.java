package co.cubicode.rbacframework.models;

import java.io.Serializable;
import java.util.Date;

import co.cubicode.jdbcframework.DatabaseObject;
import co.cubicode.rbacframework.models.catalogs.SubjectStatus;

/**
 * Clase que representa un usuario
 * 
 * @author juanku
 * 
 */
public abstract class Subject implements DatabaseObject, Serializable {

	private static final long serialVersionUID = -471768916146830103L;
	// atributos
	protected Long id;
	protected String email;
	protected String firstName;
	protected String lastName;
	protected String password;
	protected Date createDate;
	protected Date updateDate;
	protected SubjectStatus status;
	protected Date lastLogin;
	protected Role role;

	/**
	 * Verifica si un usuario tiene un rol
	 * 
	 * @param role
	 *            el rol a verificar
	 * @return verdadero en caso de tener el rol
	 */
	public boolean hasRole(Role role) {
		if (this.role == role) {
			return true;
		}
		return false;
	}

	/**
	 * Verifica si un usuario tiene un rol
	 * 
	 * @param role
	 *            el rol a verificar
	 * @return verdadero en caso de tener el rol
	 */
	public boolean hasRole(String role) {
		if (this.role.getName().equals(role)) {
			return true;
		}
		return false;
	}

	/**
	 * Verifica si un usuario tiene un permiso
	 * 
	 * @param permission
	 *            el permiso a verificar
	 * @return verdadero en caso de tener el permiso
	 */
	public boolean hasPermission(Permission permission) {
		for (Permission availablePermission : role.getPermissions()) {
			if (permission.equals(availablePermission)) {
				return true;
			}
		}
		return false;
	}

	// getters & setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public SubjectStatus getStatus() {
		return status;
	}

	public void setStatus(SubjectStatus status) {
		this.status = status;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}
